package jp.neoscorp.blecheck;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.bluetooth.BluetoothAdapter.SCAN_MODE_CONNECTABLE;
import static android.bluetooth.BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
import static android.bluetooth.BluetoothAdapter.SCAN_MODE_NONE;
import static android.bluetooth.BluetoothAdapter.STATE_OFF;
import static android.bluetooth.BluetoothAdapter.STATE_ON;
import static android.bluetooth.BluetoothAdapter.STATE_TURNING_OFF;
import static android.bluetooth.BluetoothAdapter.STATE_TURNING_ON;

public class MainActivity extends AppCompatActivity {

    private Activity mActivity;

    private static final int PERMISSION_REQUEST_CODE = 1024;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("BASE_OS", Build.VERSION.BASE_OS);
        Log.i("RELEASE", Build.VERSION.RELEASE);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert","Lets See if it Works !!!");
                Log.e("ERROR", paramThrowable.getCause().getLocalizedMessage());
                Log.e("ERROR", paramThrowable.getLocalizedMessage());
                Log.e("ERROR", paramThrowable.getStackTrace().toString());
            }
        });

        mActivity = this;
        mHandler = new Handler(Looper.getMainLooper()); //Looperでメインスレッドを指定して生成

        permissionDialog = new AlertDialog.Builder(this)
                .setTitle("アプリのアクセス権限")
                .setMessage("位置情報へのアクセスを許可してください。端末本体の\"設定\"から権限を変更できます。")
                .setPositiveButton("OK", null)
                .setNegativeButton("変更する", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // 権限画面を直接開くことはできない
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setCancelable(false)
                .create();

        initAdaptere();
    }

    private static final int TIME_TIME_START = 15 * 1000;

    @Override
    protected void onResume() {
        super.onResume();

        if (checkPermissions()) {
            //スキャン開始
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scan(null, null, true);
                }
            }, TIME_TIME_START);
        }

    }

    protected Dialog permissionDialog;

    protected boolean checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ){

                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    // 今後表示しない状態になっている
                    permissionDialog.show();
                    return false;
                }

                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, PERMISSION_REQUEST_CODE);

                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean ok = true;
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE: {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        permissionDialog.show();
                        ok = false;
                    }
                }
                break;
            }
            default:
                ok = false;
                break;
        }

        if (ok) {
            //スキャン開始
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scan(null, null, true);
                }
            }, TIME_TIME_START);

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseAdapter();
    }


    // ----------------------------

    // BLEスキャンのタイムアウト時間
    private static final long SCAN_PERIOD = 10000;

    private ArrayList<BluetoothDevice> deviceList = new ArrayList<>();
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanCallback mScanCallback;
    private Handler mHandler;

    private boolean isScanning;


    private void initAdaptere() {
        BluetoothManager bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();


        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // https://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html
                String scanMode = "";
                switch (mBluetoothAdapter.getScanMode()){
                    case SCAN_MODE_NONE: scanMode = "NONE"; break;
                    case SCAN_MODE_CONNECTABLE: scanMode = "CONNECTABLE"; break;
                    case SCAN_MODE_CONNECTABLE_DISCOVERABLE: scanMode = "CONNECTABLE_DISCOVERABLE"; break;
                }
                String state = "";
                switch (mBluetoothAdapter.getState()) {
                    case STATE_OFF: state = "OFF"; break;
                    case STATE_TURNING_ON: state = "TURNING_ON"; break;
                    case STATE_ON: state = "ON"; break;
                    case STATE_TURNING_OFF: state = "TURNING_OFF"; break;
                }

                Log.d("BLEAdapter", "isEnabled                        :" + mBluetoothAdapter.isEnabled()); //Return true if Bluetooth is currently enabled and ready for use.
                Log.d("BLEAdapter", "isDiscovering                    :" + mBluetoothAdapter.isDiscovering()); //Return true if the local Bluetooth adapter is currently in the device discovery process.
                Log.d("BLEAdapter", "isMultipleAdvertisementSupported :" + mBluetoothAdapter.isMultipleAdvertisementSupported()); //Return true if the multi advertisement is supported by the chipset
                Log.d("BLEAdapter", "isOffloadedFilteringSupported    :" + mBluetoothAdapter.isOffloadedFilteringSupported()); //Return true if offloaded filters are supported
                Log.d("BLEAdapter", "isOffloadedScanBatchingSupported :" + mBluetoothAdapter.isOffloadedScanBatchingSupported()); //Return true if offloaded scan batching is supported
                Log.d("BLEAdapter", "getName                          :" + mBluetoothAdapter.getName()); //Get the friendly Bluetooth name of the local Bluetooth adapter.
                Log.d("BLEAdapter", "getAddress                       :" + mBluetoothAdapter.getAddress()); //Returns the hardware address of the local Bluetooth adapter.
                Log.d("BLEAdapter", "getState                         :" + state); //Get the current state of the local Bluetooth adapter.
                Log.d("BLEAdapter", "getScanMode                      :" + scanMode); //Get the current Bluetooth scan mode of the local Bluetooth adapter.
            }
        }, 1000);
    }

    private void releaseAdapter() {
        mBluetoothAdapter = null;
        mBluetoothLeScanner = null;
    }

    // ScanCallbackの初期化
    private ScanCallback initCallbacks() {

        return new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                super.onScanResult(callbackType, result);
                Log.d("BLE", "onScanResult, "+ callbackType + result);

                if (result != null && result.getDevice() != null) {
                    if (isAdded(result.getDevice())) {
                        // No add
                    } else {
                        saveDevice(result.getDevice());
                    }
                }

            }


            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                super.onBatchScanResults(results);
                Log.d("BLE", "onBatchScanResults");
            }

            @Override
            public void onScanFailed(int errorCode) {

                super.onScanFailed(errorCode);
                Log.d("BLE", "onScanFailed : " + errorCode);
            }

        };

    }

    // スキャン実施
    public void scan(List<ScanFilter> filters, ScanSettings settings,
                     boolean enable) {

        mScanCallback = initCallbacks();

        if (enable) {
//            mHandler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    isScanning = false;
//                    if (mScanCallback !=null) mBluetoothLeScanner.stopScan(mScanCallback);
//                }
//            }, SCAN_PERIOD);
            Log.d("BLE", "START SCAN -------------------------------------");
            isScanning = true;
            mBluetoothLeScanner.startScan(mScanCallback);
            // スキャンフィルタを設定するならこちら
            // mBluetoothLeScanner.startScan(filters, settings, mScanCallback);
        } else {
            isScanning = false;
            if (mScanCallback !=null) mBluetoothLeScanner.stopScan(mScanCallback);
        }

    }

    // スキャン停止
    public void stopScan() {

        if (mBluetoothLeScanner != null) {
            if (mScanCallback !=null) mBluetoothLeScanner.stopScan(mScanCallback);
        }

    }

    // スキャンしたデバイスのリスト保存
    public void saveDevice(BluetoothDevice device) {

        if (deviceList == null) {
            deviceList = new ArrayList<>();
        }

        deviceList.add(device);
        Log.d("BLE", device.getAddress() + ", " + device.getName() + ", " + device.getBondState() + ", " + device.getType());
    }

    // スキャンしたデバイスがリストに追加済みかどうかの確認
    public boolean isAdded(BluetoothDevice device) {

        if (deviceList != null && deviceList.size() > 0) {
            return deviceList.contains(device);
        } else {
            return false;
        }

    }

    // ----------------------------

//    private BluetoothGatt bluetoothGatt;
//    private List<BluetoothGattService> serviceList;
//
//    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
//        @Override
//        public void onConnectionStateChange(BluetoothGatt gatt, int status,
//                                            int newState) {
//            super.onConnectionStateChange(gatt, status, newState);
//
//            // 接続成功し、サービス取得
//            if (newState == BluetoothProfile.STATE_CONNECTED) {
//                bluetoothGatt = gatt;
//                discoverService();
//            }
//
//        }
//
//        @Override
//        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
//            super.onServicesDiscovered(gatt, status);
//
//            serviceList = gatt.getServices();
//
//            for (BluetoothGattService s : serviceList) {
//                // サービス一覧を取得したり探したりする処理
//                // あとキャラクタリスティクスを取得したり探したりしてもよい
//            }
//        }
//    };
//
//    // Gattへの接続要求
//    public void connect(Context context, BluetoothDevice device) {
//
//        bluetoothGatt = device.connectGatt(context, false, mGattCallback);
//        bluetoothGatt.connect();
//
//    }
//
//    // サービス取得要求
//    public void discoverService() {
//
//        if (bluetoothGatt != null) {
//            bluetoothGatt.discoverServices();
//        }
//
//    }


}
